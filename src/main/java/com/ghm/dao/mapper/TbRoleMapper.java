package com.ghm.dao.mapper;

import com.ghm.dao.model.TbRole;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TbRoleMapper extends Mapper<TbRole> {
    long totalRole();

    List<Map<String,Object>> getAllRoleList();



}