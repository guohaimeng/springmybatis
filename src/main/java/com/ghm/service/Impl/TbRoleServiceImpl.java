package com.ghm.service.Impl;


import com.ghm.dao.mapper.TbRoleMapper;
import com.ghm.service.TbRoleService;

import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Service
public class TbRoleServiceImpl implements TbRoleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TbRoleServiceImpl.class);

    @Resource
    private TbRoleMapper tbRoleMapper;

    @Override
    public long totalRole() {
        return tbRoleMapper.totalRole();
    }

    @Override
    public List<Map<String, Object>> getAllRoleList(Integer page, Integer rows) {
        List<Map<String, Object>> allList = new ArrayList<>();
        try {
            if(null!=page&&null!=rows){
                PageHelper.startPage(page,rows);
            }
            allList = tbRoleMapper.getAllRoleList();
        }catch (Exception e){
            LOGGER.error("发生异常！",e);
        }
        return allList;
    }


}
