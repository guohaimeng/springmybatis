package com.ghm.service;

import java.util.List;
import java.util.Map;

public interface TbRoleService {
    long totalRole();

    List<Map<String,Object>> getAllRoleList(Integer page, Integer rows);
}
