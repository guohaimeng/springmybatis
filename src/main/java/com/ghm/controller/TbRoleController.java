package com.ghm.controller;


import com.ghm.service.TbRoleService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/role/")
public class TbRoleController {

    @Resource
    private TbRoleService tbRoleService;

    @RequestMapping(value = "all", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public  Object getAllRoleList(Integer page,Integer rows){
        Map<String,Object> testMap = new HashMap<>();
        testMap.put("total",tbRoleService.totalRole());
        testMap.put("rows",tbRoleService.getAllRoleList(page,rows));
        testMap.put("errcode",0);
        return  testMap;
    }
}
